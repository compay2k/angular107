import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DogsComponent } from './components/dogs/dogs.component';
import { PanierDetailsComponent } from './components/panier-details/panier-details.component';
import { FicheDogComponent } from './components/fiche-dog/fiche-dog.component';
import { MovieSearchComponent } from './components/movie-search/movie-search.component';
import { MovieDetailComponent } from './components/movie-detail/movie-detail.component';


const routes: Routes = [
    {path:'chiens', component:DogsComponent},
    {path:'panier', component:PanierDetailsComponent},
    {path:'chiens/:idDuChien', component:FicheDogComponent},
    {path:'movies', component:MovieSearchComponent},
    {path:'movies/:id', component:MovieDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }