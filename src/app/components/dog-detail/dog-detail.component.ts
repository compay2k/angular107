import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Dog } from '../../entities/dog';
import { PanierService } from 'src/app/services/panier.service';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-dog-detail',
  templateUrl: './dog-detail.component.html',
  styleUrls: ['./dog-detail.component.css']
})
export class DogDetailComponent implements OnInit, OnChanges {

  @Input() dog : Dog;
  quantite : number = 0;

  ajout()
  {
    this.messageService.add("Article ajouté au panier");
    this.panierService.ajouter(this.quantite, this.dog);
  }

  constructor(private panierService : PanierService,
              private messageService : MessageService) { 
   
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.quantite = 0;
  }

}
