import { Component, OnInit } from '@angular/core';
import { Panier } from 'src/app/entities/panier';
import { PanierService } from 'src/app/services/panier.service';

@Component({
  selector: 'app-panier-details',
  templateUrl: './panier-details.component.html',
  styleUrls: ['./panier-details.component.css']
})
export class PanierDetailsComponent implements OnInit {

  panier : Panier;

  constructor(private svc : PanierService) { }

  ngOnInit() {
    // je récupère le panier auprès du service panier :
    this.panier = this.svc.panier;
  }

}
