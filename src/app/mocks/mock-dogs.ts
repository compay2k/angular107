import { Dog } from '../entities/dog'; 

  export const DOGS : Dog[] = [
    {id : 1, nom : "Idéfix", prixUnitaire : 25, promotion : 0},
    {id : 2, nom : "Rantanplan", prixUnitaire : 42, promotion : 0},
    {id : 3, nom : "Bill", prixUnitaire : 64, promotion : 0},
    {id : 4, nom : "Pif", prixUnitaire : 50, promotion : 0.3},
    {id : 5, nom : "Pluto", prixUnitaire : 78, promotion : 0},
    {id : 6, nom : "Rintintin", prixUnitaire : 100, promotion : 0.2},
];