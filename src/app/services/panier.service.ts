import { Injectable } from '@angular/core';
import { Panier } from '../entities/panier';
import { Dog } from '../entities/dog';
import { PanierLigne } from '../entities/panier-ligne';

@Injectable({
  providedIn: 'root'
})
export class PanierService {

panier : Panier = new Panier();

constructor() { }

ajouter(qte : number, dog:Dog) : void {

  for (const ligne of this.panier.lignes) {
    if (ligne.dog === dog)
    {
      ligne.quantite += qte;
      return;
    }
  }

  this.panier.lignes.push({quantite:qte, dog:dog});

  console.log(this.panier);

  /*
  // classique :
  let ligne1 = new PanierLigne();
  ligne1.quantite = qte;
  ligne1.dog = dog;
  this.panier.lignes.push(ligne1);

  // json :
  let ligne2 : PanierLigne = {
    quantite:qte,
    dog:dog
  };
  this.panier.lignes.push(ligne2);
*/

} 

getQuantiteTotale() : number
{
  let result = 0;
  for (const ligne of this.panier.lignes) {
    result += ligne.quantite;  
  }
  return result;
}

}
